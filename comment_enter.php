<?php

session_start();
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }
    
    if(!is_null($_SESSION['username'])){ //checks if there is a user before entering their comments
    //$title = $_GET['mystorytitle']; //the title of the story that the user commented on
    $commentbody = $_GET['commentbody']; //the comment


    // inserting the commenter id (the user who is logged in) and the comment into the comments table
    $comment = $mysqli->prepare("insert into comments (commenter_id, comment_body, com_story_title) values (?, ?, ?)");
     if(!comment) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $comment->bind_param('sss', $_SESSION['username'], $commentbody, $_SESSION['mystorytitle']);
    $comment->execute();
    $comment->close();


    
    // inserting the comment body into the story that it was commented on
    $storycomment = $mysqli->prepare("update stories set comments=concat(comments, ?) where story_title = ?");
     if(!storycomment) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $storycomment->bind_param('ss', $commentbody, $_SESSION['mystorytitle']);
    $storycomment->execute();
    $storycomment->close();
    header("location:home_page.php");
    }
    else {
        header('Refresh: 2; URL=http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/home_page.php');
        echo "You're not logged in, you can't submit a comment!!";
    }
    
    
?>