<?php
session_start();
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }
    
    if(!is_null($_SESSION['username'])){ //checks if there is a logged in user
    $commenter_id;
    $comment_id=$_GET['editcomment'];
    $_SESSION['commentid'] = $comment_id;
   
    
    $author = $mysqli->prepare("SELECT commenter_id FROM comments WHERE comment_id=?");//gets author of the story
    if(!$author) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    };
    
    $author->bind_param('s', $comment_id);
    $author->execute();
    $author->store_result();
    $author->bind_result($commenter_id);
    $author->fetch();
   
    if ((strcmp($commenter_id, $_SESSION['username'])) == 0 or $_SESSION['username'] == "admin"){
       
    header("location:edit_comment2.php");
    }
    else{
        header('Refresh: 2; URL=http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/home_page.php');
        echo "You can't edit comment you didn't write!!";
    }
}
    else{
    header('Refresh: 2; URL=http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/home_page.php');
        echo "You're not logged in, you can't edit comments!!!!";
}
?>