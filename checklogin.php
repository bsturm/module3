<?php

    session_start();
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }
     // Define $myusername and $mypassword as whatever the person entered in the fields
    $myusername=$_POST['myusername']; 
    $mypassword=$_POST['mypassword'];
    $_SESSION['username'] = $myusername;
      
      // To protect MySQL injection (more detail about MySQL injection)
    $myusername = stripslashes($myusername);
    $mypassword = stripslashes($mypassword);
    $myusername = mysql_real_escape_string($myusername);
    $mypassword = mysql_real_escape_string($mypassword);
    $stored_pass;
    
    $hashpass = $mysqli->prepare("SELECT account_pass FROM accounts WHERE account_id=?");
    if(!hashpass) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    };
    
    $hashpass->bind_param('s', $myusername);
    $hashpass->execute();
    $hashpass->store_result();
    $hashpass->bind_result($stored_pass);
    $hashpass->fetch();

   
    // saves the entered password as the hashed password
    $mypassword = crypt($mypassword, $stored_pass);
    
    $stmt = $mysqli->prepare("SELECT account_id, account_pass FROM accounts WHERE account_id=? and account_pass=?");
    if(!stmt) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('ss', $myusername, $mypassword);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($myusername, $mypassword);
    $stmt->fetch();
 
    
    // Mysql_num_row is counting table row
    echo $stmt->num_rows;
    // If result matched $myusername and $mypassword, table row must be 1 row
    if($stmt->num_rows == 1){

    // Register $myusername, $mypassword and redirect to file "login_success.php"
    session_register("myusername");
    session_register("mypassword"); 
    
   
    header("location:home_page.php");
    }else {
        echo "Wrong Username or Password";
        echo "    $myusername";
        echo "    $mypassword";
    }
    
?>