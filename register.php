<?php
    session_start();
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }
    
    // Define $myusername and $mypassword as whatever the person entered in the fields
    $myusername=$_POST['myusername']; 
    $mypassword=$_POST['mypassword'];
    
    //statement to try to insert the entered username and password-hash
    $stmt = $mysqli->prepare("insert into accounts (account_id, account_pass) values (?,?)");
    if(!stmt) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('ss', $myusername, crypt($mypassword));
    $stmt->execute();
    $stmt->close();
    header("location:login_page.html");
    
?>