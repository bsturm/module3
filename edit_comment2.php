<?php
session_start();
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }
   
    
    //get the comment body, story it, then put it in a textfield for the user to edit
    $comment_body;
    
    $body = $mysqli->prepare("SELECT comment_body FROM comments WHERE comment_id=?");//gets author of the story
    if(!$body) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    };
    
    $body->bind_param('s', $_SESSION['commentid']);
    $body->execute();
    $body->bind_result($comment_body);
    $body->fetch();

    echo('<form name="submit_comment" method="get" action="edit_comment3.php"><!-- button to go enter comment -->
					
				Comment:		<textarea name="commentbody" id="commentbody" cols="50" rows="15">'.$comment_body.'</textarea>
				
						<input type="submit" name="Submit5" value="Submit Edit">');
?>