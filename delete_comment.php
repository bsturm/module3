<?php

session_start();
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }

    
    if(!is_null($_SESSION['username'])){ //checks if there is a user before entering their comments
    $com_author;
    $delete_num=$_GET['deletecomment'];
    
    $author = $mysqli->prepare("SELECT commenter_id FROM comments WHERE comment_id=?");//gets author of the comment
    if(!$author) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    };
    
    $author->bind_param('i', $delete_num);
    $author->execute();
    $author->store_result();
    $author->bind_result($com_author);
    $author->fetch();
    
    if ($com_author == $_SESSION['username']){
         $stmt = $mysqli->prepare("delete from comments where comment_id = ?"); //removes comment
    if(!$stmt) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param("i", $delete_num);
    $stmt->execute();
    $stmt->close();
    header("location:home_page.php");
    }
    else{
        header('Refresh: 2; URL=http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/home_page.php');
        echo "You can't delete comments you didn't write!!";
    }
}
else{
    header('Refresh: 2; URL=http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/home_page.php');
        echo "You're not logged in, you can't delete comments!!";
    }

    
    
    
    
    
    
    
?>