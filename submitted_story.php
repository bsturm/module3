<?php

 session_start();
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }
    
    // Define $storytitle and $storybody as whatever the person entered in the fields
    $storytitle=$_GET['mystorytitle']; 
    $storybody=$_GET['mystorybody'];
    $loggeduser = $_SESSION['username']; //the account_id of the user that is logged in
    //this should be the link to the story (except fix spaces from the $storytitle variable)
    $storytitle1 = str_replace(' ', '%20', $storytitle);
    $link = "http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/view_story.php?mystorytitle=$storytitle1";
    //this puts the story title, story body, and author id into the stories table and then brings us
    //to the story
    $stmt = $mysqli->prepare("insert into stories (story_title, story, author_id, story_link) values (?,?,?,?)");
    if(!stmt) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('ssss', $storytitle, $storybody, $loggeduser, $link);
    $stmt->execute();
    $stmt->close();
  
    
    header("location:view_story.php?mystorytitle=$storytitle");
    

    

    

?>