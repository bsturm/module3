<?php

session_start();
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }
    
    if(!is_null($_SESSION['username'])){ //checks if there is a logged in user
    $story_author;
    $story_title=$_GET['editstory'];
    $_SESSION['editstory'] = $story_title;
    
    $author = $mysqli->prepare("SELECT author_id FROM stories WHERE story_title=?");//gets author of the story
    if(!$author) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    };
    
    $author->bind_param('s', $story_title);
    $author->execute();
    $author->store_result();
    $author->bind_result($story_author);
    $author->fetch();
   
    if ((strcmp($story_author, $_SESSION['username'])) == 0){
       
    header("location:story_edit.php");
    }
    else{
        header('Refresh: 2; URL=http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/home_page.php');
        echo "You can't edit stories you didn't write!!";
    }
}
    else{
    header('Refresh: 2; URL=http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/home_page.php');
        echo "You're not logged in, you can't edit stories!!";
}


?>