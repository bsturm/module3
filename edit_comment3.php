<?php


session_start(); 
   
    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="newsite"; // Database name 
    $tbl_name="accounts"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli($host, $username, $password, $db_name); 
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }



$commentbody = $_GET['commentbody'];

// update the new comment body of the comment
    $storycomment = $mysqli->prepare("update comments set comment_body = ? where comment_id = ?");
     if(!$storycomment) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $storycomment->bind_param('ss', $commentbody,  $_SESSION['commentid']);
    $storycomment->execute();
    $storycomment->close();
    header("location:home_page.php");


?>